///////////////////////////////////////////////////////////////////// Sketch 2 >>>
/**
 * REF : https://opentype.js.org/index.html
 * 
 */

 

 let font // opentype.js font object
 let fSize // font size
 let msg // text to write
 //let pts = [] // store path data
 let path;
 let x = 20;
 let y = 300;
 let isPnts, isFill = false;
 let isOutline = true;
 let isVertex = false;
 let snapX, snapY = 0;
 let snapStrength;
 let snapDistance;
 let varOne, varTwo;
 let sinAngle_01;
 let isAnime = false;
 let isGUI = false;
 let svg;
 let cnv;


 function pathToSVG(){
     var fileName = "vectorPath.svg"
     var url = "data:image/svg+xml;utf8," + encodeURIComponent(svg);
     var link = document.createElement("a");
     link.download = fileName;
     link.href = url;
     link.click();
   }

 /////////////////////////// SETUP ////////////////////////////
 function setup() {
      cnv = createCanvas(innerWidth, innerHeight);
     cnv.parent('theCanvas2');
     
     background(0);
     smooth();
     
     var fontFileName = 'IBMPlexMono-Bold.otf';
    // document.getElementById('file').innerHTML = fontFileName;

    // var fileSelect = document.getElementById('file');
    // fileSelect.addEventListener('change', onReadFile, false);

     opentype.load('/font/IBMPlexMono-Bold.otf', function (err, f) {
         if (err) {
             alert('Font could not be loaded: ' + err);
         } else {
             font = f;
             fSize = 270;
             msg = 'type...';

             path = font.getPath(msg, x, y, fSize);
             //console.log(path);
             path.fill = 'blue';
         }
     })
     button_4.value = false;
 }

 /////////////////////////// DRAW ////////////////////////////
 function draw() {

     fSize = slider1.value;
     snapStrength = slider2.value;
     
     snapDistance = slider3.value;
     varOne = slider4.value;
     varTwo = slider5.value;
     sinAngle_01 = slider5.value;
     //isAnime = slider6.value;


     translate(0, 200);
     if (!font) return
     background(0);
     if (isOutline) {
         drawFontOutline();
     }
     
     if(isVertex){
         drawFontVertex();
     }
     if (isPnts) {
         drawCommandPoints();
         legend();
     }
     path = font.getPath(msg, x, y, fSize);
     //doSnap(path);
     doSnapAlgo2( path );

     //path = font.getPath(msg, x, y, fSize);
     //svg = path.toSVG(2, 940, 480);
 
 };



 // >>>>>>>>> FONT OUTLINE
  function drawFontOutline(){
     noFill();
     stroke(255);
     strokeWeight(1.75);
     for (let cmd of path.commands) {       
         // Move To: Move to a new position. This creates a new contour
         if (cmd.type === 'M') {
             beginShape();
             vertex(cmd.x, cmd.y);
             //Line To: Draw a line from the previous position to the given coordinate. 
         } else if (cmd.type === 'L') {
             vertex(cmd.x, cmd.y);
             //Curve To: Draw a bézier curve from the current position to the given coordinate.
         } else if (cmd.type === 'C') {
             bezierVertex(cmd.x1, cmd.y1, cmd.x2, cmd.y2, cmd.x, cmd.y);
             //Quad To: Draw a quadratic bézier curve from the current position to the given coordinate. 
         } else if (cmd.type === 'Q') {
             quadraticVertex(cmd.x1, cmd.y1, cmd.x, cmd.y);
             //Close: Close the path.
         } else if (cmd.type === 'Z') {
             endShape(CLOSE);
         }
     }
 }

 // >>>>>>>>> FONT COMMANDS
 function drawCommandPoints(){
     let indx = 0;
     noStroke();
         for (let cmd of path.commands) {
             if (cmd.type === 'M') {
                 push();
                 fill(255, 0, 0);
                 ellipse(cmd.x, cmd.y, 10, 10);
                 pop();

             } else if (cmd.type === 'L') {
                 push();
                 fill(0, 0, 255);
                 ellipse(cmd.x, cmd.y, 10, 10);
                 pop();

             } else if (cmd.type === 'Z') {

             } else if (cmd.type === 'C') {
                 push();
                 fill(0, 255, 0);
                 ellipse(cmd.x, cmd.y, 10, 10);
                 pop();

             } else if (cmd.type === 'Q') {

             } else if (cmd.type === 'Z') {

             }
         }
 }

     // >>>>>>>>> FONT OUTLINE
      function drawFontVertex(){
         if(isFill){
             fill(245,200,0);
         }else{
             noFill();
             
         }
         stroke(255);
         strokeWeight(1.75);
         for (let cmd of path.commands) {
             // Move To: Move to a new position. This creates a new contour
             if (cmd.type === 'M') {
                 beginShape();
                 fill(245,200,0);
                 vertex(cmd.x, cmd.y);
                 //Line To: Draw a line from the previous position to the given coordinate. 
             } else if (cmd.type === 'L') {
                 vertex(cmd.x, cmd.y);
                 //Curve To: Draw a bézier curve from the current position to the given coordinate.
             } else if (cmd.type === 'C') {
                 //p.bezierVertex(cmd.x1, cmd.y1, cmd.x2, cmd.y2, cmd.x, cmd.y);
                 vertex(cmd.x, cmd.y);
                 //p.curveVertex(cmd.x, cmd.y);
                 //Quad To: Draw a quadratic bézier curve from the current position to the given coordinate. 
             } else if (cmd.type === 'Q') {
                 //p.curveVertex(cmd.x, cmd.y);
                 //p.quadraticVertex(cmd.x1, cmd.y1, cmd.x, cmd.y);
                 //Close: Close the path.
             } else if (cmd.type === 'Z') {
                 vertex(cmd.x, cmd.y);
                 endShape(CLOSE);
             }
         }
     }

 // >>>>>>>>> INFO COMMMANDS
 function legend() {
     noStroke();
     // NEW CONTOUR ==== M
     push();
     fill(255);
     textSize(15);
     text('M = New Contour', 135, 450);
     pop();
     push();
     fill(255, 0, 0);
     ellipse(115, 445, 10, 10);
     pop();

     //  DRAW LINE TO ==== L
     push();
     fill(0, 0, 255);
     //textSize(12);
     text('L = Draw line to', 280, 450);
     pop();
     push();
     fill(0, 0, 255);
     ellipse(265, 445, 10, 10);
     pop();

     // NEW CURVE TO ==== C
     push();
     fill(0, 255, 0);
     textSize(15);
     text('C = Draw curve to', 425, 450);
     pop();
     push();
     fill(0, 255, 0);
     ellipse(405, 445, 10, 10);
     pop();
 }

 
 function keyPressed() {
     if (keyCode == BACKSPACE) {
         if (msg.length > 0)
             msg = msg.substring(0, msg.length - 1);
     } else if ((keyCode != BACKSPACE) && (keyCode != SHIFT)) {
         msg = msg + key;
     }
 }



// >>>>>>>>> TRANSFORM FUNCTIONS
 // Round a value to the nearest "step".
 function snap(v, distance, strength) {
     return (v * (1.0 - strength)) + (strength * Math.round(v / distance) * distance);
 }

 function snapXCos(v, d, strength) {
     //return (v * (1.0)) + (strength * Math.cos(v*0.015)*d);
     return (v * (1.0)) + (strength * Math.cos(v*0.015)*d);
 }
 
 function snapYSin(v, d, strength) {
     //return (v * (1.0)) + (strength * Math.sin(v*0.015)*d);
     return (v * (1.0)) + (strength * Math.sin(v*0.015)*d);
 }

 // we are modifying the path commands for the font
 // Q:quadratic bez | C:curveTo | Z:close path | M:moveTo | L:LineTo
 function doSnap(_path) {
     //console.log(_path);
     var i;
     let strength = snapStrength / 50.0;
     for (i = 0; i < _path.commands.length; i++) {
         var cmd = _path.commands[i];
     
         if (cmd.type !== 'Z') {
             cmd.x = snap(cmd.x , snapDistance, strength);
             cmd.y = snap(cmd.y, snapDistance, strength);
         }
         
         if (cmd.type === 'Q' || cmd.type === 'C') {
             cmd.x = snap(cmd.x , snapDistance, strength);
             cmd.y = snap(cmd.y, snapDistance, strength);
         }
 
         if (cmd.type === 'C') {
             cmd.x = snap(cmd.x , snapDistance, strength);
             cmd.y = snap(cmd.y, snapDistance, strength);
         }            
         
     }
 }


 function doSnapAlgo2(_path) {
     var i;
     let strength = snapStrength / 50.0;
     for (i = 0; i < _path.commands.length; i++) {
         var cmd = _path.commands[i];
     
         if (cmd.type !== 'Z') {
             cmd.x = snap(cmd.x , snapDistance, strength);
             cmd.y = snap(cmd.y, snapDistance, strength);
         }
         
         if (cmd.type === 'Q' || cmd.type === 'C') {
             cmd.x = snap(cmd.x , snapDistance, strength);
             cmd.y = snap(cmd.y, snapDistance, strength);
         }
 
         if (cmd.type === 'C') {
             //cmd.x = snap(cmd.x , snapDistance, strength);
             //cmd.y = snap(cmd.y, snapDistance, strength);
             //cmd.x += Math.cos((cmd.x + cmd.y) + sinAngle_01*0.0015) * varOne;
             //cmd.y += Math.sin((cmd.x + cmd.y) + sinAngle_01*0.0015) * varOne;
             if(isAnime){
                 var xx = Math.cos((cmd.x + cmd.y) + frameCount*(sinAngle_01*0.002)) * varOne;
                 cmd.x = snap(cmd.x + xx , snapDistance, strength);
                 cmd.y = snap(cmd.y + xx, snapDistance, strength);
             }else {
                 var xx = Math.cos((cmd.x + cmd.y) + sinAngle_01*0.015) * varOne;
                 cmd.x = snap(cmd.x + xx , snapDistance, strength);
                 cmd.y = snap(cmd.y + xx, snapDistance, strength);
             }
         }
         if (cmd.type === 'L') {
             //cmd.x += Math.cos((cmd.x + cmd.y) + sinAngle_01*0.0015) * varOne;
             //cmd.y += Math.sin((cmd.x + cmd.y) + sinAngle_01*0.0015) * varOne;
             if(isAnime){
                 var xx = Math.cos((cmd.x + cmd.y) + frameCount*(sinAngle_01*0.002)) * varOne;
                 cmd.x = snap(cmd.x + xx , snapDistance, strength);
                 cmd.y = snap(cmd.y + xx, snapDistance, strength);
             }else {
                 var xx = Math.cos((cmd.x + cmd.y) + sinAngle_01*0.015) * varOne;
                 cmd.x = snap(cmd.x + xx , snapDistance, strength);
                 cmd.y = snap(cmd.y + xx, snapDistance, strength);
             }
         }
         
     }
 }

 // GUI updates:
 function outputUpdateSlider1(v) {
     document.querySelector('#fontSizes').value = v;
 }
 function outputUpdateSlider2(v) {
     document.querySelector('#snapStrength').value = v;
 }
 function outputUpdateSlider3(v) {
     document.querySelector('#snapDistance').value = v;
 }
 function outputUpdateSlider4(v) {
     document.querySelector('#varOne').value = v;
 }
 function outputUpdateSlider5(v) {
     document.querySelector('#varTwo').value = v;
 }
 


 function outputUpdateSlider7(v) {
     document.querySelector('#button_1').value = v;
 }

 function msg1() {
     isPnts = !isPnts;
     button_1.value = isPnts;
 }
 function outputUpdateSlider8(v) {
     document.querySelector('#button_2').value = v;
 }
 function msg2() {
     isOutline = !isOutline;
     button_2.value = isOutline;
 }
 function outputUpdateSlider9(v) {
     document.querySelector('#button_3').value = v;
 }
 function msg3() {
     isVertex = !isVertex;
     button_3.value = isVertex;
 }
 function outputUpdateSlider10(v) {
     document.querySelector('#button_4').value = v;
 }

 function msg4() {
     isAnime = !isAnime;
     button_4.value = isAnime;
 }

 function guiUpdate(v){
     console.log(document.querySelector('#prmgtitle').value);
 }


 // font file input

 function onReadFile(e) {
     //document.getElementById('font-name').innerHTML = '';
     var file = e.target.files[0];
     var reader = new FileReader();
     reader.onload = function(e) {
         try {
             font = opentype.parse(e.target.result);
             //onFontLoaded(font);
             console.log("We have new forms. Yummy!");
         } catch (err) {
             showErrorMessage(err.toString());
         }
     };
     /*
       reader.onerror = function(err) {
           showErrorMessage(err.toString());
       };
     */
     reader.readAsArrayBuffer(file);
 }


 function keyTyped(){   
         if(key === 'a'){
         isGUI = !isGUI;
         
         let v = document.querySelector('#prmgtitle');
         if(isGUI){
            v.style.display = 'none';
         }else{
            v.style.display = 'block';
         }
        }
 }